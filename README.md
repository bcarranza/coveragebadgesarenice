# Introduction


[![pipeline status](https://gitlab.com/bcarranza/coveragebadgesarenice/badges/troubleshooting/pipeline.svg)](https://gitlab.com/bcarranza/coveragebadgesarenice/-/commits/troubleshooting) | [![coverage report](https://gitlab.com/bcarranza/coveragebadgesarenice/badges/troubleshooting/coverage.svg)](https://gitlab.com/bcarranza/coveragebadgesarenice/-/commits/troubleshooting) | ![Website](https://img.shields.io/website?down_color=lightgrey&down_message=sad&up_color=green&up_message=happy&url=https%3A%2F%2Fbcarranza.gitlab.io%2Fcoveragebadgesarenice)


The value above is the average code coverage for all jobs. Below are badges showing you the coverage value for each job:

Job `test_code_coverage`: [![coverage report](https://gitlab.com/bcarranza/coveragebadgesarenice/badges/troubleshooting/coverage.svg?job=test_code_coverage)](https://gitlab.com/bcarranza/coveragebadgesarenice/-/commits/troubleshooting)

Job `more_testing_coverage`: [![coverage report](https://gitlab.com/bcarranza/coveragebadgesarenice/badges/troubleshooting/coverage.svg?job=more_testing_coverage)](https://gitlab.com/bcarranza/coveragebadgesarenice/-/commits/troubleshooting)


Under normal circumstances, the badges above should give you useful information about the state of pipelines and code coverage in this repository. As this is for testing purposes, your mileage may vary. 

I have set up GitLab Pages for this project, it's visible at [https://bcarranza.gitlab.io/coveragebadgesarenice/](https://bcarranza.gitlab.io/coveragebadgesarenice/).

## About Code Coverage
You can [query for the code coverage of an individual job](https://about.gitlab.com/blog/2016/11/03/publish-code-coverage-report-with-gitlab-pages/) like so:

 
 First, grab the pre-populated coverage report Markdown syntax:

> Settings -> CI/CD > Scroll to 'Coverage report'
 
 Next, append `?job=job_name` to the URL for the `.svg` file.

Then, add that value to the Markdown file you would like it to be rendered in. 

## Behind the Scenes
There are two things that we need to think about here:

  - [Test coverage parsing](https://docs.gitlab.com/ee/ci/pipelines/settings.html#test-coverage-parsing) - This is what extracts the test coverage percentage.
  - [Pipeline badges](https://docs.gitlab.com/ee/ci/pipelines/settings.html#pipeline-badges) - These are the nice badges you see above!



## Useful Links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Publish Code Coverage Report with GitLab Pages](https://about.gitlab.com/blog/2016/11/03/publish-code-coverage-report-with-gitlab-pages/)
- [Angular Code Coverage Badge with GitLab CI](https://calebukle.com/blog/angular-code-coverage-badge-with-gitlab-ci)

## External Badges
There are a few places that offer custom badges:

  - [shields.io](https://shields.io/)


## Was this useful?
  - **Yes**: `Mon Oct  5 10:23:07 EDT 2020`



